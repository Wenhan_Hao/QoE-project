# -*- coding: utf-8 -*-
"""
Created on Thu Jul 19 11:36:43 2018

@author: yoji
"""

import parser.parser as prs
import parser_compact.parser as prs2


# These contain mapping from int<->string for
# whatever answers the workers provided.
dict_step_2, dict_step_3, dict_rating = prs.make_dicts()

# Contains a list of parsed dataframes
# in a more minimised format
#
# [stripped the columns with answers in string-format
#  because they're redundant now that
#  the dictionaries have been created.]
results_dataframes = prs.parse_results()

# UPDATED FOR COMPACTNESS.
results = prs2.parse_results()

# COLUMNS AND MEANING BELOW for prs2:
"""
'VideoList' --> Order of videos, as per video_list_x.csv files
'Worker ID'
'Finished'
'Begin Date'
'Start Time'
'End Date'
'End Time'
'Step 2' --> List containing answers for step2 questions
'Step 3' --> List containing answers for step3 questions
'Durations_15'
'Duration_s' --> Duration for first 4 ratings
'Ratings' --> List containing answers for all ratings
'Duration_v'
'Order'  --> Order in which steps have been presented
"""

DMOS, percent_kicked = prs2.calc_dmos(results)
