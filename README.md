# QoE Subjective Quality Testing
Provided in this repository is our group's implementation of a subjective quality testing model, based on the survey data received from MTurk on video quality.

## Group Members and Task Distribution

**Yanglu Zeng**
Interpretation
Conclusions


**Efia Prodan**   
Plotting demographics data  
Plotting DMOS against demographics

**Wenhan**   
Feature extraction using Netflix (VMAF, SSIM, etc)  
Predictive model building(PCR, PLSR)  
Cross Validation(LeaveOneContentOut, LeaveOneOut)  

**Yuankai Wu**    
Presentation and tidying of results

**Iosif Andrei**   
Parsing of MTurk data   
DMOS calculation   
Interface for selective DMOS calculation (for plotting purposes)

## Brief overview of implementation
Testing .csv's are reduced to only numerical data. Their meaning is still contained in dictionary structures so as to map back to the original string values for easier interpretation in later stages.   
   
Demographics and streaming habits are inspected visually through the use of graphs.   

Features of input videos from the subjective testing have been extracted with VMAF to serve as input data for our predictive model.

## Project Structure
```
qoe18-group4
│   README.md
│   main.py      
│
│# MTurk files, renamed for 
|# easier sorting and parsing
└───files_mturk
│   │   # files related to information
│
│# Documentation folder
└───docs
│   │   # files related to information
│   │   # about implementation choice
|   |   # and presentation-related stuff.
│
│# Parsing of MTurk data
└───parser
│   │   parser.py  # (Joseph)
│
[To be continued]
```
