# -*- coding: utf-8 -*-
"""
Data Parsing Script

Collates functions to process
the .csv files from MTurk

@author: yoji
"""

import numpy as np
import pandas as pd
import os

# Path adjustment in case
# we run the module separately
if __name__ == '__main__':

    print("parser run separately")
    FILEPATH = "../files_mturk/"
    PARENT = "../parser"
else:
    print("parser imported.")
    FILEPATH = "files_mturk/"
    PARENT = ".."


# ------- EXTRACTING QUESTIONS AND ANSWERS FOR DICTIONARY CREATION --------- #
QUESTIONS = pd.read_csv(FILEPATH+"0_survey_questions.csv")
QUESTIONS.answers = QUESTIONS.answers.str.split("; ")


def make_dicts():
    """
    Creates dictionaries
    for easier interpretation.
    """
    # Dictionaries for mapping int's to their literals for steps 2, 3
    dict_step_2 = []
    dict_step_3 = []

    for index, row in QUESTIONS.iterrows():
        localdict = {}
        listvals = row.answers
        listvals = [x.split(": ") for x in listvals]

        for item in listvals:
            localdict[int(item[0])] = item[1]

        if row.step == 2:
            dict_step_2.append(localdict)
        else:
            dict_step_3.append(localdict)

    #
    # Dictionary for mapping integers to literal strings
    dict_rating = {1: 'Bad',
                   2: 'Poor',
                   3: 'Fair',
                   4: 'Good',
                   5: 'Excellent'}

    return (dict_step_2, dict_step_3, dict_rating)


# ---------------------------------------------------------------------------#

def Result(i):
    """
    Proper naming requires
    no duplicate colnames
    """
    result = "Result " + str(i)

    return result


# ---------------------------------------------------------------------------#

def minimise(dataframe):
    """
    Converts data to numerical-only
    representation.
    """
    # list of indexes where numerical representation of data is stored
    # idx_id = [0, 1, 2, 3, 4, 5]  # worker ID, stuff like that
    idx_step_2 = [9, 11, 13, 15]
    idx_step_3 = list(np.arange(start=18, stop=34, step=2))
    idx_steps_stab = [36, 39, 42, 45]
    idx_steps_videos = list(np.arange(start=49, stop=103, step=3))
    idx_step_order = [101]

    #
    # Indexes storing durations of steps
    idx_durations_13 = [6, 7, 16, 33, 34]
    idx_duration_s = [37, 40, 43, 46]
    idx_duration_v = list(np.arange(start=47, stop=99, step=3))

    colnames = ['Worker ID',
                'Finished',
                'Begin Date',
                'Start Time',
                'End Date',
                'End Time',
                'Step 2',
                'Step 3',
                'Durations_13',
                'Stabilisation',
                'Duration_s',
                'Videos',
                'Duration_v',
                'Order']

    df_min = pd.DataFrame(columns=colnames)

    for _, row in dataframe.iterrows():

        df_min = df_min.append({'Worker ID': row[0],
                                'Finished': row[1],
                                'Begin Date': row[2],
                                'Start Time': row[3],
                                'End Date': row[4],
                                'End Time': row[5],
                                'Step 2': list(row[idx_step_2]),
                                'Step 3': list(row[idx_step_3]),
                                'Durations_13': list(row[idx_durations_13]),
                                'Stabilisation': list(row[idx_steps_stab]),
                                'Duration_s': list(row[idx_duration_s]),
                                'Videos': list(row[idx_steps_videos]),
                                'Duration_v': list(row[idx_duration_v]),
                                'Order': list(row[idx_step_order])},
                               ignore_index=True)
    return df_min

# ---------------------------------------------------------------------------#

# In[]:


def parse_results():
    """
    Parses the one.csv ... eight.csv files
    and returns minimised versions of them
    """
    df_list = []

    file_content = os.listdir(FILEPATH)
    os.chdir(FILEPATH)
    file_content.sort()
    file_content = file_content[1:9]

    for file in file_content:
        result = pd.read_csv(file, header=None)
        result = result.iloc[2:]  # strip "header"
        result.reset_index(inplace=True, drop=True)  # fix bad indexing

        df_list.append(minimise(result))

    os.chdir(PARENT)  # move back into dir we came from

    # merge them and indicate video list origin
    result = pd.concat(df_list, keys=[1, 2, 3, 4, 5, 6, 7, 8])
    result = result.reset_index(level=0)
    result.rename(columns={"level_0": 'VideoList'},
                  inplace=True)  # fix the pandas shenanigans

    return result

# ---------------------------------------------------------------------------#

# In[]:

# Ignore this, it's for testing while
# writing the code. All useful functions
# are to be imported and used in main.py
if __name__ == '__main__':
    RUN_IT = True
    results_dataframes = parse_results()
    dict_step_2, dict_step_3, dict_rating = make_dicts()

else:
    RUN_IT = False  # don't ask.
# In[]:

# Step Indexes for Videos
# (same in every vidlist)
vid1_ref = [10, 14]
vid1_1234 = [[11, 15],
             [16, 19],
             [17, 18],
             [20]]
#
vid2_ref = [12, 21]
vid2_1234 = [[13, 22, 26],
             [23, 27],
             [24],
             [25]]


# Pairs
videos = [["CrowdRun", "ElFuente"],
          ["ElFuente", "FoxBird"],
          ["FoxBird", "Seeking"],
          ["Boats", "Seeking"],
          ["Boats", "Cow"],
          ["Cow", "Food"],
          ["Food", "People"],
          ["Crowd", "People"]]

# Dict to map back into names
dict1 = {1: "CrowdRun",
         2: "ElFuente",
         3: "FoxBird",
         4: "Boats",
         5: "Cow",
         6: "Food",
         7: "Seeking",
         8: "People"}
