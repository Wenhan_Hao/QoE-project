#!/bin/bash

#f=VMAF
#f=SSIM
f=MSSIM
#f=PSNR

cd /home/wenhan/vmaf

# colour_boats
g=colour_boats
./run_vmaf yuv420p 854 480 qoe_project_video_data/colour_boats/yuv/colour_boats_30fps_scaled_0RP_cut1.yuv qoe_project_video_data/colour_boats/yuv/colour_boats_30fps_scaled_1RP_cut1.yuv --out-fmt json > qoe_project/features/${f}_${g}1.json
./run_vmaf yuv420p 854 480 qoe_project_video_data/colour_boats/yuv/colour_boats_30fps_scaled_0RP_cut1.yuv qoe_project_video_data/colour_boats/yuv/colour_boats_30fps_scaled_2RP_cut1.yuv --out-fmt json > qoe_project/features/${f}_${g}2.json
./run_vmaf yuv420p 854 480 qoe_project_video_data/colour_boats/yuv/colour_boats_30fps_scaled_0RP_cut1.yuv qoe_project_video_data/colour_boats/yuv/colour_boats_30fps_scaled_3RP_cut1.yuv --out-fmt json > qoe_project/features/${f}_${g}3.json
./run_vmaf yuv420p 854 480 qoe_project_video_data/colour_boats/yuv/colour_boats_30fps_scaled_0RP_cut1.yuv qoe_project_video_data/colour_boats/yuv/colour_boats_30fps_scaled_4RP_cut1.yuv --out-fmt json > qoe_project/features/${f}_${g}4.json

# cow
g=cow
./run_vmaf yuv420p 854 480 qoe_project_video_data/cow/yuv/cow_30fps_scaled_0RP.yuv qoe_project_video_data/cow/yuv/cow_30fps_scaled_1RP.yuv --out-fmt json > qoe_project/features/${f}_${g}1.json
./run_vmaf yuv420p 854 480 qoe_project_video_data/cow/yuv/cow_30fps_scaled_0RP.yuv qoe_project_video_data/cow/yuv/cow_30fps_scaled_2RP.yuv --out-fmt json > qoe_project/features/${f}_${g}2.json
./run_vmaf yuv420p 854 480 qoe_project_video_data/cow/yuv/cow_30fps_scaled_0RP.yuv qoe_project_video_data/cow/yuv/cow_30fps_scaled_3RP.yuv --out-fmt json > qoe_project/features/${f}_${g}3.json
./run_vmaf yuv420p 854 480 qoe_project_video_data/cow/yuv/cow_30fps_scaled_0RP.yuv qoe_project_video_data/cow/yuv/cow_30fps_scaled_4RP.yuv --out-fmt json > qoe_project/features/${f}_${g}4.json

# crowdrun
g=crowdrun
./run_vmaf yuv420p 854 480 qoe_project_video_data/crowdrun/yuv/CrowdRun_25fps_scaled_0RP.yuv qoe_project_video_data/crowdrun/yuv/CrowdRun_25fps_scaled_1RP.yuv --out-fmt json > qoe_project/features/${f}_${g}1.json
./run_vmaf yuv420p 854 480 qoe_project_video_data/crowdrun/yuv/CrowdRun_25fps_scaled_0RP.yuv qoe_project_video_data/crowdrun/yuv/CrowdRun_25fps_scaled_2RP.yuv --out-fmt json > qoe_project/features/${f}_${g}2.json
./run_vmaf yuv420p 854 480 qoe_project_video_data/crowdrun/yuv/CrowdRun_25fps_scaled_0RP.yuv qoe_project_video_data/crowdrun/yuv/CrowdRun_25fps_scaled_3RP.yuv --out-fmt json > qoe_project/features/${f}_${g}3.json
./run_vmaf yuv420p 854 480 qoe_project_video_data/crowdrun/yuv/CrowdRun_25fps_scaled_0RP.yuv qoe_project_video_data/crowdrun/yuv/CrowdRun_25fps_scaled_4RP.yuv --out-fmt json > qoe_project/features/${f}_${g}4.json

# elfuente
g=elfuente
./run_vmaf yuv420p 854 480 qoe_project_video_data/elfuente/yuv/ElFuente1_30fps_scaled_0RP.yuv qoe_project_video_data/elfuente/yuv/ElFuente1_30fps_scaled_1RP.yuv --out-fmt json > qoe_project/features/${f}_${g}1.json
./run_vmaf yuv420p 854 480 qoe_project_video_data/elfuente/yuv/ElFuente1_30fps_scaled_0RP.yuv qoe_project_video_data/elfuente/yuv/ElFuente1_30fps_scaled_2RP.yuv --out-fmt json > qoe_project/features/${f}_${g}2.json
./run_vmaf yuv420p 854 480 qoe_project_video_data/elfuente/yuv/ElFuente1_30fps_scaled_0RP.yuv qoe_project_video_data/elfuente/yuv/ElFuente1_30fps_scaled_3RP.yuv --out-fmt json > qoe_project/features/${f}_${g}3.json
./run_vmaf yuv420p 854 480 qoe_project_video_data/elfuente/yuv/ElFuente1_30fps_scaled_0RP.yuv qoe_project_video_data/elfuente/yuv/ElFuente1_30fps_scaled_4RP.yuv --out-fmt json > qoe_project/features/${f}_${g}4.json

# food_market
g=food_market
./run_vmaf yuv420p 854 480 qoe_project_video_data/food_market/yuv/food_market_30fps_scaled_0RP_cut1.yuv qoe_project_video_data/food_market/yuv/food_market_30fps_scaled_1RP_cut1.yuv --out-fmt json > qoe_project/features/${f}_${g}1.json
./run_vmaf yuv420p 854 480 qoe_project_video_data/food_market/yuv/food_market_30fps_scaled_0RP_cut1.yuv qoe_project_video_data/food_market/yuv/food_market_30fps_scaled_2RP_cut1.yuv --out-fmt json > qoe_project/features/${f}_${g}2.json
./run_vmaf yuv420p 854 480 qoe_project_video_data/food_market/yuv/food_market_30fps_scaled_0RP_cut1.yuv qoe_project_video_data/food_market/yuv/food_market_30fps_scaled_3RP_cut1.yuv --out-fmt json > qoe_project/features/${f}_${g}3.json
./run_vmaf yuv420p 854 480 qoe_project_video_data/food_market/yuv/food_market_30fps_scaled_0RP_cut1.yuv qoe_project_video_data/food_market/yuv/food_market_30fps_scaled_4RP_cut1.yuv --out-fmt json > qoe_project/features/${f}_${g}4.json

# foxbird
g=foxbird
./run_vmaf yuv420p 854 480 qoe_project_video_data/foxbird/yuv/Foxbird_25fps_scaled_0RP.yuv qoe_project_video_data/foxbird/yuv/Foxbird_25fps_scaled_1RP.yuv --out-fmt json > qoe_project/features/${f}_${g}1.json
./run_vmaf yuv420p 854 480 qoe_project_video_data/foxbird/yuv/Foxbird_25fps_scaled_0RP.yuv qoe_project_video_data/foxbird/yuv/Foxbird_25fps_scaled_2RP.yuv --out-fmt json > qoe_project/features/${f}_${g}2.json
./run_vmaf yuv420p 854 480 qoe_project_video_data/foxbird/yuv/Foxbird_25fps_scaled_0RP.yuv qoe_project_video_data/foxbird/yuv/Foxbird_25fps_scaled_3RP.yuv --out-fmt json > qoe_project/features/${f}_${g}3.json
./run_vmaf yuv420p 854 480 qoe_project_video_data/foxbird/yuv/Foxbird_25fps_scaled_0RP.yuv qoe_project_video_data/foxbird/yuv/Foxbird_25fps_scaled_4RP.yuv --out-fmt json > qoe_project/features/${f}_${g}4.json

# people
g=people
./run_vmaf yuv420p 854 480 qoe_project_video_data/people/yuv/people_30fps_scaled_0RP.yuv qoe_project_video_data/people/yuv/people_30fps_scaled_1RP.yuv --out-fmt json > qoe_project/features/${f}_${g}1.json
./run_vmaf yuv420p 854 480 qoe_project_video_data/people/yuv/people_30fps_scaled_0RP.yuv qoe_project_video_data/people/yuv/people_30fps_scaled_2RP.yuv --out-fmt json > qoe_project/features/${f}_${g}2.json
./run_vmaf yuv420p 854 480 qoe_project_video_data/people/yuv/people_30fps_scaled_0RP.yuv qoe_project_video_data/people/yuv/people_30fps_scaled_3RP.yuv --out-fmt json > qoe_project/features/${f}_${g}3.json
./run_vmaf yuv420p 854 480 qoe_project_video_data/people/yuv/people_30fps_scaled_0RP.yuv qoe_project_video_data/people/yuv/people_30fps_scaled_4RP.yuv --out-fmt json > qoe_project/features/${f}_${g}4.json

# seeking
g=seeking
./run_vmaf yuv420p 854 480 qoe_project_video_data/seeking/yuv/Seeking_25fps_scaled_0RP.yuv qoe_project_video_data/seeking/yuv/Seeking_25fps_scaled_1RP.yuv --out-fmt json > qoe_project/features/${f}_${g}1.json
./run_vmaf yuv420p 854 480 qoe_project_video_data/seeking/yuv/Seeking_25fps_scaled_0RP.yuv qoe_project_video_data/seeking/yuv/Seeking_25fps_scaled_2RP.yuv --out-fmt json > qoe_project/features/${f}_${g}2.json
./run_vmaf yuv420p 854 480 qoe_project_video_data/seeking/yuv/Seeking_25fps_scaled_0RP.yuv qoe_project_video_data/seeking/yuv/Seeking_25fps_scaled_3RP.yuv --out-fmt json > qoe_project/features/${f}_${g}3.json
./run_vmaf yuv420p 854 480 qoe_project_video_data/seeking/yuv/Seeking_25fps_scaled_0RP.yuv qoe_project_video_data/seeking/yuv/Seeking_25fps_scaled_4RP.yuv --out-fmt json > qoe_project/features/${f}_${g}4.json
